<?php

namespace App\Http\Controllers;

use App\Http\Requests\FeedStoreRequest;
use App\Models\Feed;
use App\Services\Flickr\Repositories\FeedRepository;
use App\Services\Flickr\Resources\FeedResources;
use Illuminate\Http\Request;

class FeedController extends ApiController
{
    private $repository;

    public function __construct(FeedRepository $feedRepository)
    {
        $this->repository = $feedRepository;
    }

    public function index(Request $request)
    {
        //sleep($request->delay ?? 0);

        $data = $this->repository->all([
            'lang' => 'en-us',
            'format' => 'json',
            'tags' => $request->q,
        ]);

        return new FeedResources($data['items']);
    }


    public function rating(FeedStoreRequest $request)
    {
        Feed::updateOrCreate([
            'author_id' => $request->input('author_id'),
            'date_taken' => $request->input('date_taken'),
        ], [
            'rating' => $request->input('rating')
        ]);
    }
}
