<?php


namespace App\Services\Flickr\DataAccess;


use Illuminate\Support\Facades\Http;

class FeedDataAccess extends DataAccess
{
    const URL = "https://api.flickr.com/services/feeds/photos_public.gne";

    public function all(array $filters = [])
    {
        return Http::get(self::URL, $filters + ['nojsoncallback' => 1])->throw()->json();
    }
}
