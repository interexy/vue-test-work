<?php

namespace App\Services\Flickr\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class FeedResources extends ResourceCollection
{

    public function toArray($request)
    {
        return [
            'data' => FeedResource::collection($this->collection->sortBy([
                ['published', 'desc']
            ])),
            'tags' => $this->getTags(),
        ];

    }


    public function getTags()
    {
        $tags = [];
        $result = [];
        $inc = 0;

        foreach ($this->collection as $item) {
            $tags = array_merge($tags, explode(' ', $item['tags']));
        }

        $tags = array_count_values($tags);

        uasort($tags, function ($a, $b) {
            return ($a == $b ? 0 : $a < $b) ? 1 : -1;
        });

        array_walk($tags, function ($key, $item) use (&$result, &$inc) {
            $result[] = [
                'id' => $inc++,
                'name' => $item,
                'count' => $key,
            ];
        });

        return $result;
    }
}
