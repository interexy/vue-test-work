<?php

namespace App\Services\Flickr\Resources;

use App\Models\Feed;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class FeedResource extends JsonResource
{
    static $inc = 0;

    public function toArray($request)
    {
        return [
            'id' => self::$inc++,
            'title' => $this['title'],
            'author_id' => $this['author_id'],
            'date_taken' => $this['date_taken'],
            'images' => $this['media'],
            'published' => (new Carbon($this['published']))->diffForHumans(),
            'author' => $this['author'],
            'description' => $this['description'],
            'rating' => $this->getRating()
        ];
    }


    public function getRating()
    {
        return Feed::query()
                ->where('author_id', $this['author_id'])
                ->where('date_taken', $this['date_taken'])
                ->select('rating')
                ->first()->rating ?? 0;
    }

}
