<?php


namespace App\Services\Flickr\Repositories;


use App\Services\Flickr\DataAccess\FeedDataAccess;
use App\Services\Flickr\Resources\FeedResource;
use App\Services\Flickr\Resources\FeedResources;

class FeedRepository
{
    private $dataAccess;

    public function __construct(FeedDataAccess $feedDataAccess)
    {
        $this->dataAccess = $feedDataAccess;
    }

    public function all(array $params = [])
    {
        return $this->dataAccess->all($params);
    }
}
