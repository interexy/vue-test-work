**Build a Vue.js app that can fetch a batch of photo data from the Flickr api and display the images in a grid.**

---

## Requirements

The page should have a single text input where a user can enter any search term. This is the tag that will be passed to the Flickr api.

- The page should display a loading graphic OR disabled buttons / input fields during state transitions.
- The photos should be sorted by the "published" property, from newest to oldest.
- Display the elapsed time since the "published" date for each item, in any format ("2 days ago", "13 minutes ago", etc). This can be rendered normally or in a tooltip.
- The app should suggest the next search based on the most common tags shared by all of the current results, if they exist. Example: If I search "Hawaii", the most common tags among the results are "beach" and "surfing". Suggest these in the form of links or buttons below the search bar.
- While the request is in flight, the user should be given the option to cancel it and stay on the current search. This button should not be the same as the search button.


---

## Extra

- Build an "upvote" backend that allows you to 1) Give a +1/-1 to any result, and 2) Shows the current tally for that item (if any) when the page loads. You can move the call to the Flickr API to the backend if you do this.

---

## API Url

-  https://api.flickr.com/services/feeds/photos_public.gne?lang=en-us&format=json&tags=

---

**Создайте приложение Vue.js, которое может извлекать пакет данных о фотографиях из Flickr api и отображать изображения в виде сетки.**

---

## Требования

На странице должен быть единственный текстовый ввод, в который пользователь может ввести любой поисковый запрос. Это тег, который будет передан в API Flickr.

- На странице должна отображаться загружаемая графика ИЛИ отключенные кнопки / поля ввода во время переходов между состояниями.
- Фотографии должны быть отсортированы по параметру «опубликовано» от самых новых до самых старых.
- Отображение времени, прошедшего с даты «публикации» для каждого элемента в любом формате («2 дня назад», «13 минут назад» и т. Д.). Это может отображаться в обычном режиме или во всплывающей подсказке.
- Приложение должно предлагать следующий поиск на основе наиболее распространенных тегов, общих для всех текущих результатов, если они существуют. Пример. Если я ищу «Гавайи», наиболее частыми тегами среди результатов являются «пляж» и «серфинг». Предложите их в виде ссылок или кнопок под строкой поиска.
- Пока запрос находится в полете, пользователю должна быть предоставлена возможность отменить его и продолжить текущий поиск. Эта кнопка не должна совпадать с кнопкой поиска.

---

## Дополнительный

- Создайте бэкэнд "upvote", который позволяет вам 1) давать + 1 / -1 любому результату и 2) показывать текущий счет для этого элемента (если есть) при загрузке страницы. Если вы это сделаете, вы можете переместить вызов Flickr API в серверную часть.

---

## API Url

-  https://api.flickr.com/services/feeds/photos_public.gne?lang=en-us&format=json&tags=

---

