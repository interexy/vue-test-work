require('./bootstrap');
window.Vue = require('vue');

import Vue from 'vue'
import VueRouter from 'vue-router'
import FlickrFinder from "./components/FlickrFinder";

Vue.use(VueRouter)

Vue.component('flickr-finder', require('./components/FlickrFinder.vue').default);


const router = new VueRouter({

    routes: [
        {
            path: '/',
            component: FlickrFinder,
            name: 'finder'
        },
        {
            path: '/:tag',
            component: FlickrFinder,
            name: 'finder-tag'
        },
    ]
})

const app = new Vue({
    el: '#app',
    router:router
});

